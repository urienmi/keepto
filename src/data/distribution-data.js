const distributionData = {
  type: "doughnut",
  data: {
    labels: ['Red', 'Orange', 'Yellow', 'Green', 'Blue'],
    datasets: [
      {
        label: 'Dataset 1',
        data: 5,
        backgroundColor: 'red',
      }
    ]
  },
  options: {
    responsive: true,
    plugins: {
      legend: {
        position: "right",
      },
    },
  },
};

export default distributionData;
